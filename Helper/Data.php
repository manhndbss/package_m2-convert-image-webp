<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_ConvertImageWebp
 * @author     Extension Team
 * @copyright  Copyright (c) 2021 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\ConvertImageWebp\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use WebPConvert\Convert\Exceptions\ConversionFailedException;
use WebPConvert\WebPConvert;

/**
 * Class Data
 */
class Data extends AbstractHelper
{
    const FOLDER_IMAGE ="bss/webp/";

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $filesystem;

    /**
     * Data constructor.
     * @param Context $context
     */
    public function __construct(
        \Magento\Framework\Filesystem $filesystem,
        Context $context
    ) {
        $this->filesystem = $filesystem;
        parent::__construct($context);
    }

    /**
     * Create a file image webp from other image
     *
     * @param string $sourceImageFilename
     * @param string $destinationImageFilename
     * @throws ConversionFailedException
     */
    public function convertWebp(string $sourceImageFilename, string $destinationImageFilename): void
    {
        WebPConvert::convert($sourceImageFilename, $destinationImageFilename, $this->getOptions());
    }

    /**
     * Options when convert
     *
     * @return array
     */
    public function getOptions(): array
    {
        return [
            'quality' => 'auto',
            'max-quality' => $this->getQualityImageWebp(),
            'converters' => ['cwebp', 'gd', 'imagick', 'wpc', 'ewww'],
        ];
    }

    /**
     * Get quality image webp
     *
     * @return int
     */
    public function getQualityImageWebp()
    {
        return (int)$this->scopeConfig->getValue('c_i_w/general/quality');
    }

    /**
     * Convert image webp
     *
     * @param string $sourceImageUri
     * @param string|null $destinationImageUri
     * @return bool
     * @throws \Exception
     */
    public function convert(string $sourceImageUri, ?string $destinationImageUri = null)
    {
        $rootFolder = $this->filesystem->getDirectoryRead(DirectoryList::ROOT)->getAbsolutePath();
        if (!$destinationImageUri) {
            $destinationImageUri = preg_replace('/\.(jpg|jpeg|png|gif)$/', '.webp', $sourceImageUri);
        }

        $destinationImageUri = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath()
            . self::FOLDER_IMAGE . substr($destinationImageUri, strlen($rootFolder));

        try {
            $this->convertWebp($sourceImageUri, $destinationImageUri);
        } catch (ConversionFailedException $e) {
            throw new \Exception($destinationImageUri . ': ' . $e->getMessage());
        }

        return true;
    }

    /**
     * Write log
     *
     * @param string $message
     */
    public function writeLog($message)
    {
        $this->_logger->critical($message);
    }
}
