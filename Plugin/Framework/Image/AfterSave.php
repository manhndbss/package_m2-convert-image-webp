<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_ConvertImagesWebp
 * @author     Extension Team
 * @copyright  Copyright (c) 2021 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\ConvertImageWebp\Plugin\Framework\Image;

use Magento\Framework\Image;

/**
 * Class AfterSave
 */
class AfterSave
{
    /**
     * @var \Bss\ConvertImageWebp\Helper\Data
     */
    protected $helperData;

    /**
     * AfterSave constructor.
     *
     * @param \Bss\ConvertImageWebp\Helper\Data $helperData
     */
    public function __construct(
        \Bss\ConvertImageWebp\Helper\Data $helperData
    ) {
        $this->helperData = $helperData;
    }

    /**
     * Convert image webp when Admin upload image
     *
     * @param Image $subject
     * @param string|null $destination
     * @param string|null $newFileName
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterSave(Image $subject, $result, $destination = null, $newFileName = null)
    {
        try {
            $this->helperData->convert((string)$destination);
        } catch (\Exception $e) {
            $this->helperData->writeLog($e->getMessage());
        }
    }
}
